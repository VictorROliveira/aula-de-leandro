#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    float nota1, nota2, nota3, peso1, peso2, peso3, media;
    printf("Programa: Media ponderada. \n\n");
    printf("Insira o valor da primeira nota: ");
    scanf("%f", &nota1);
    printf("Insira o peso da primeira nota: ");
    scanf("%f", &peso1);
    printf("Insira o valor da segunda nota: ");
    scanf("%f", &nota2);
    printf("Insira o peso da segunda nota: ");
    scanf("%f", &peso2);
    printf("Insira o valor da terceira nota: ");
    scanf("%f", &nota3);
    printf("Insira o peso da terceira nota: ");
    scanf("%f", &peso3);
    media = (nota1*peso1 + nota2*peso2 + nota3*peso3)/(peso1+peso2+peso3);
    
    if (media >= 6){
    	printf("Aprovado!");
	} else {
		printf("Reprovado!");
	}
    printf("A media : %.2f \n\n", media);
    
    
    

    system("pause");
    return 0;
}

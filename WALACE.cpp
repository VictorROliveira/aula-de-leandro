#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<locale.h>

int Menu(){ //função para imprimir os printfs.
	
	int num; 
	
	printf("Digite um numero entre 0 e 9");
	printf("Digite 0 para sair \n" 
			"[1] Para verificação de numero par ou impar. \n" 
			"[2] Para descobrir uma potência \n" 
			"[3] Para descobrir uma raiz \n" 
			"[4] Para verificar se o ano é bissexto \n" 
			"[5] Para descobrir a média ponderada de 3 notas \n"
			"[6] Para descobrir a média \n" 
			"[7] Para verificação de duas medias \n"
			"[8] Para checar matricula e nome em Hexadecimal \n"
			"[9] Para verificar se o número é primo \n"
			"[10] \n" );
	scanf("%d", &num);
	return(num);
	}
void ParImpar(){
	int num2;
	int resto;
		printf("Digite um numero: \n");
		scanf("%d", &num2);
		resto=num2%2;
	if(resto == 0)
	{
		printf("Esse numero e par \n");
	}else if(resto !=0)
		printf("Esse numero e impar \n");
}
void Potencia(){
	float numA, resultado;
	int numB;
		printf("Digite um numero com vírgula: ");
		scanf("%f", &numA);
		printf("Digite um numero inteiro: ");
		scanf("%d", &numB);
		resultado = pow(numA, numB);
		printf("Valor da potência: 	%.2e \n", resultado );
	
}
void Raiz(){
	float numA1, resultado1;
	int numB1;
		printf("Digite um numero de ponto flutuante: ");
		scanf("%f", &numA1);
		printf("Digite um numero inteiro: ");
		scanf("%d", &numB1);
		resultado1 = pow(numA1, 1./numB1);
		printf("Valor da raiz: 	%.2e \n", resultado1);
}
void Bissexto(){
	int ano,bissexto;
		printf(" Digite um ano para descobrir se ele é bissexto:");
		scanf("%d", &ano);
		bissexto = ano % 4;
	if(bissexto==0){
		printf("Esse ano é bissexto \n");
	} else{
		printf("Esse ano não é bissexto \n");
}
}
void Ponderada(){
	float nota1, nota2, nota3, peso1, peso2, peso3, media;
    	printf("Programa: Media ponderada. \n\n");
        printf("Insira o valor da primeira nota: ");
        scanf("%f", &nota1);
        printf("Insira o peso da primeira nota: ");
        scanf("%f", &peso1);
    	printf("Insira o valor da segunda nota: ");
        scanf("%f", &nota2);
    	printf("Insira o peso da segunda nota: ");
        scanf("%f", &peso2);
        printf("Insira o valor da terceira nota: ");
        scanf("%f", &nota3);
        printf("Insira o peso da terceira nota: ");
        scanf("%f", &peso3);
        media = (nota1*peso1 + nota2*peso2 + nota3*peso3)/(peso1+peso2+peso3);
    if (media >= 6){
        printf("Aprovado!");
    } else {
        printf("Reprovado!");
    }
        printf("A media : %.2f \n\n", media);
}
void Media(){
		float nota1, nota2,media;
		printf("Digite a primeira nota");
		scanf("%f", &nota1);
	if (nota1>=0 && nota1<=10)
	{
		printf("Digite a Segunda nota");
		scanf("%f", &nota2);
	if (nota2 >= 0 && nota2 <= 10)
	{
		media= (nota1 + nota2)/2;
		printf("A média é: %.2f \n", media);
}
}
}
void VerificacaoMedia(){
	float nota1, nota2, media;
        printf("Digite as duas notas para descobrir a media:\n ");
        printf("Digite a primeira nota: ");
        scanf("%f", &nota1);
        printf("Digite a segunda nota: ");
        scanf("%f", &nota2);
	if ((nota1 < 0) || (nota1 > 10) && (nota2 < 0) || (nota2 > 10)) {
        printf("Nota inserida invalida, tente novamente depois.");
    } else {
        media = (nota1 + nota2)/2;
        printf("Notas validas, a media e: %.2f", media);
	}
}
void Hexadecimal(){
	int matricula;
    char nomeAluno;
		printf("Digite sua matricula: ");
        scanf("%d", &matricula);
        printf("Digite o nome do aluno: ");
        scanf("%s", &nomeAluno);
        printf("A matricula em hexadecimal e: %X", matricula);
        printf("O nome do aluno em hexadecimal e: %X", nomeAluno);
}
void Primo(){
	int num, i, resultado = 0;
        printf("digite um numero: ");
        scanf("%d", &num);
	for (i = 2; i <= num/2; i++) {
    if (num % i == 0) {
        resultado++;
	break;
    }
    }
    if (resultado == 0){
        printf("E um numero primo!\n", num);
    } else {
    	printf("Nao e um numero primo!\n", num);
    }
        scanf("%d", num);
}
	int main()
	{
		int num;
		//modo para poder utilizar acentos nas palavras.
		setlocale(LC_ALL, "Portuguese");
	do
	{	//chamada da função.
		num=Menu();
		
		system("CLS");
		//condição para continuidade do menu.		
	if(num>0 && num<11)
	{//loop em forma de switch para entra em cada caso no menu.
	switch (num)
	{
	case 1 :
	{
		ParImpar();
	break;
	}
	case 2 :
	{
		Potencia();
	break;
	}
	case 3 :
	{
		Raiz();
	break;
	}
	case 4 :
	{
		Bissexto();
	break;
	}
	case 5 :
	{
        	Ponderada();
	break;
	}
	case 6 :
	{
		Media();
	break;
	}
	case 7 :
	{
		VerificacaoMedia();
	break;
	}
	case 8 :
	{
		Hexadecimal();
 	break;
	}
	case 9 :
	{
		Primo();
	break;
	}
	case 10:
	{		
	break;
	}
	default:
		printf("Valor Inexistente \n");
	break;
	}
	}
	else if(num<0 || num>10 )
	{
		printf("Digite um numero valido");
	}
	else 
	{
		printf("Tchau!");
	}
		
	}//condição para término do menu.
	while(num!=0);
	
}


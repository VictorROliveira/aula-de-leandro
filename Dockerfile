#include <stdio.h>

int main() {
  int N;
  int n100, n50, n20, n10, n5, n2, n1;
  int r100, r50, r20, r10, r5, r2, r1;

  scanf("%d", &N);

  n100 = N / 100;
  r100 = N % 100;

  n50 = r100 / 50;
  r50 = r100 % 50;
  
  n20 = r50 / 20;
  r20 = r50 % 20;
  
  n10 = r20 / 10;
  r10 = r20 % 10;
 
  n5 = r10 / 5;
  r5 = r10 % 5;

  n2 = r5 / 2;
  r2 = r5 % 2;
  
  n1 = r5 / 1;
  r1 = r5 % 1;
  
  printf("%d\n", N);
  if (n100>0){
    printf("%d nota(s) de R$ 100,00\n", n100);
  } else if (n100==0){
    printf("%d nota(s) de R$ 100,00\n", n100);
  }
  if (n50>0){
    printf("%d nota(s) de R$ 50,00\n", n50);
  } else if (n50==0){
    printf("%d nota(s) de R$ 50,00\n", n50);
  }
  if (n20>0){
    printf("%d nota(s) de R$ 20,00\n", n20);
  } else if (n20==0){
    printf("%d nota(s) de R$ 20,00\n", n20);
  }
  if (n10>0){
    printf("%d nota(s) de R$ 10,00\n", n10);
  } else if (n10==0){
    printf("%d nota(s) de R$ 10,00\n", n10);
  }
  if (n5>0){
    printf("%d nota(s) de R$ 5,00\n", n5);
  } else if (n5==0){
    printf("%d nota(s) de R$ 5,00\n", n5);
  }
  if (n2>0){
    printf("%d nota(s) de R$ 2,00\n", n2);
  } else if (n2==0){
    printf ("%d nota(s) de R$ 2,00\n", n2);
  }
  if (n1>0){
    printf("%d nota(s) de R$ 1,00", n1);
  } else if (n1==0){
    printf("%d nota(s) de R$ 1,00", n1);
  }
  printf("\n");
  return 0;
}
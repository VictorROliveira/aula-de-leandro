#include <stdio.h>

struct local {
	char rua[100];
	int numero;
	char cep[9];
};
struct pessoa{
	char nome[100];
	int idade;
	struct local endereco;
};
int main (){
	
	struct pessoa dados;
	
	printf("Digite um nome: ");
	scanf("%s", &dados.nome);
	printf("Digite uma idade: ");
	scanf("%d", &dados.idade);
	printf("Digite um endereco: ");
	scanf("%s", &dados.endereco.rua);
	printf("Digite o numero: ");
	scanf("%d", &dados.endereco.numero);
	printf("Digite o CEP: ");
	scanf("%s", &dados.endereco.cep);
	
	printf("Nome:%s\n", dados.nome);
	printf("Idade:%d\n", dados.idade);
	printf("Rua:%s\n", dados.endereco.rua);
	printf("Numero:%d\n", dados.endereco.numero);
	printf("CEP:%s", dados.endereco.cep);

}

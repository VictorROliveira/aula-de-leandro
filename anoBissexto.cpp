#include <stdio.h>
#include <stdlib.h>

int main() 
{
	int ano;
	printf("Digite um ano: ");
	scanf("%d", &ano);
	
	if (ano % 4 == 0) {
		printf("Esse e um ano bissexto!");
	} else {
		printf("Esse nao e um ano bissexto.");
	}
}
